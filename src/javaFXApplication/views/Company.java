package javaFXApplication.views;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import java.io.IOException;

public class Company extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        try {
            Parent root = FXMLLoader.load(getClass().getResource("Index.fxml"));
            primaryStage.setTitle("Administración de Usuarios");
            primaryStage.getIcons().add(new Image(Company.class.getResourceAsStream("asset/img/ico.png")));
            primaryStage.setScene(new Scene(root, 900, 540));
            primaryStage.setResizable(false);
            primaryStage.sizeToScene();
            primaryStage.show();

        } catch (IOException e) {

            e.getMessage();
            e.getCause();
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}
