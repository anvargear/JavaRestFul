package javaFXApplication.controllers;

import javaFXApplication.entities.Customer;
import javaFXApplication.entities.CustomerTblModel;
import javaFXApplication.model.util.StageUtil;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import javafx.stage.Modality;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;



public class IndexController implements Initializable {


    private final IndexController THIS = this;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private Button buttonSearch;
    @FXML
    private TableView<Customer> tableView;

    @FXML private TableView<CustomerTblModel> customersTable;


    @FXML
    public void handleSearchAction(){

        WebTarget clientTarget;
        ObservableList<Customer> data = tableView.getItems();
        data.clear();
        Client client = ClientBuilder.newClient();
        client.register(CustomerMessageBodyReader.class);
        if (textFieldSearch.getText().length() > 0) {
            clientTarget = client.target("http://www.tecnicasapi.app/api/users/{user}");
            clientTarget = clientTarget.resolveTemplate("user", textFieldSearch.getText());
        } else {
            clientTarget = client.target("http://www.tecnicasapi.app/api/users");
        }
        GenericType<List<Customer>> listc = new GenericType<List<Customer>>() {
        };
        List<Customer> customers = clientTarget.request("application/json").get(listc);

        for (Customer c : customers) {
            data.add(c);
            System.out.println(c.toString());
        }

    }

    @FXML
    public void updateAction(){
        StageUtil.loadEditCustomer(THIS, Modality.APPLICATION_MODAL).showAndWait();
    }

    @FXML
    public void removeAction(){
        StageUtil.loadEditCustomer(THIS, Modality.APPLICATION_MODAL).showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        handleSearchAction();
    }

    @FXML
    private void newAction() {
        StageUtil.loadNewCustomer(THIS, Modality.APPLICATION_MODAL).showAndWait();
    }
    public TableView<CustomerTblModel> getTable() {
        return customersTable;
    }
}
