package javaFXApplication.controllers;

import java.text.ParseException;

import javaFXApplication.entities.Customer;
import javaFXApplication.model.util.DateUtil;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public abstract class AbstractCustomerController {

    protected Customer buildCustomer() throws ParseException {
        Customer customer = new Customer();
        customer.setName(getTxtName().getText());
        customer.setUsername(getTxtUsername().getText());
        customer.setType(getTxtType().getText());
        customer.setEmail(getTxtEmail().getText());
        customer.setCreated_at(DateUtil.format("yyyy-MM-dd", new java.util.Date()));

        return customer;
    }
    protected void closeStage() {
        ((Stage) getTxtName().getScene().getWindow()).close();
    }


    /* **********************************
     *      GETTERS AND SETTERS
     **********************************/
    public abstract TextField getTxtName();
    public abstract void setTxtName(TextField txtName);
    public abstract TextField getTxtUsername();
    public abstract void setTxtUsername(TextField txtUsername);
    public abstract TextField getTxtType();
    public abstract void setTxtType(TextField txtType);
    public abstract TextField getTxtEmail();
    public abstract void setTxtEmail(TextField txtEmail);

}
