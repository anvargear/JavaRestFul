package javaFXApplication.controllers;

import javaFXApplication.entities.Customer;
import javaFXApplication.entities.CustomerTblModel;
import javaFXApplication.model.util.Convert;
import javaFXApplication.model.util.DialogUtil;
import javaFXApplication.model.util.FormUtil;

import java.text.ParseException;
import java.time.LocalDate;

import javaFXApplication.model.util.rest.RESTClient;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;


public class EditCustomerController extends AbstractCustomerController {

    @FXML private TextField txtName;
    @FXML private TextField txtUsername;
    @FXML private TextField txtEmail;
    @FXML private TextField txtType;
    private IndexController fatherGUI;

    public void setFatherGUI(IndexController fatherGUI) {
        this.fatherGUI = fatherGUI;
        showData();
    }
    @FXML private void updateAction() {
        try {
            Customer updated = null;
            if(FormUtil.isValid(txtName, txtUsername, txtEmail, txtType)) {
                updated = updateCustomer();
                closeStage();
                updateUserInTable(Convert.toCustomerTblModel(updated));
            } else {
                DialogUtil.buildSimpleDialog("Importante", null, "No deje campos vacíos", AlertType.WARNING).showAndWait();
            }
        } catch(RuntimeException | ParseException e) {
            DialogUtil.buildExceptionDialog
                    ("Ha ocurrido un error", "Error al actualizar usuario", e).showAndWait();
        }
    }

    @FXML private void cancelAction() {
        closeStage();
    }

    private Customer updateCustomer() throws RuntimeException, ParseException {
        Customer customerToUpdate = buildCustomer();
        customerToUpdate.setId(getSelected().getId());
        Customer updated = RESTClient.updateCustomer(customerToUpdate);
        return updated;
    }


    private void showData() {
        CustomerTblModel selected = getSelected();
        this.getTxtName().setText(selected.getName());
        this.getTxtUsername().setText(selected.getUsername());
        this.getTxtEmail().setText(selected.getEmail());
        this.getTxtType().setText(selected.getType());
    }
    private CustomerTblModel getSelected() {
        return fatherGUI.getTable().getSelectionModel().getSelectedItem();
    }
    private void updateUserInTable(CustomerTblModel customerTblModel) {
        CustomerTblModel selected = fatherGUI.getTable().getSelectionModel().getSelectedItem();
        ObservableList<CustomerTblModel> customerList = fatherGUI.getTable().getItems();
        customerList.remove(selected);
        customerList.add(customerTblModel);
    }

    @Override
    public TextField getTxtName() {
        return txtName;
    }

    @Override
    public void setTxtName(TextField txtName) {
        this.txtName = txtName;
    }

    @Override
    public TextField getTxtUsername() {
        return txtUsername;
    }

    @Override
    public void setTxtUsername(TextField txtUsername) {
        this.txtUsername = txtUsername;
    }

    @Override
    public TextField getTxtEmail() {
        return txtEmail;
    }

    @Override
    public void setTxtEmail(TextField txtEmail) {
        this.txtEmail = txtEmail;
    }

    @Override
    public TextField getTxtType() {
        return txtType;
    }

    @Override
    public void setTxtType(TextField txtType) {
        this.txtType = txtType;
    }
}
