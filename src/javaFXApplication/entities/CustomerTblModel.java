package javaFXApplication.entities;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class CustomerTblModel {

    final private IntegerProperty id;
    final private StringProperty name;
    final private StringProperty username;
    @SuppressWarnings("unused")
    final private StringProperty email;
    final private StringProperty type;

    public CustomerTblModel() {
        this.id = new SimpleIntegerProperty();
        this.name = new SimpleStringProperty();
        this.username = new SimpleStringProperty();
        this.email = new SimpleStringProperty();
        this.type = new SimpleStringProperty();
    }

    public CustomerTblModel(IntegerProperty id, StringProperty name, StringProperty username, StringProperty email, StringProperty type) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.type = type;
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getUsername() {
        return username.get();
    }

    public StringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getType() {
        return type.get();
    }

    public StringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }
}
