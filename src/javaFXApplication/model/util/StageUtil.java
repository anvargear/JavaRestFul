package javaFXApplication.model.util;

import javaFXApplication.controllers.IndexController;
import javaFXApplication.views.Company;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.ws.rs.core.Application;
import java.io.IOException;

public class StageUtil extends Application{

    public static Stage loadNewCustomer(final IndexController FATHER, final Modality MODALITY) {
        Stage stage = new Stage();
        try {
            final FXMLLoader LOADER = new FXMLLoader();
            LOADER.setLocation(Company.class.getResource("NewCustomer.fxml"));
            AnchorPane root = LOADER.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("Nuevo cliente");
            stage.getIcons().add(new Image(Company.class.getResourceAsStream("asset/img/ico.png")));
            stage.initModality(MODALITY);
            stage.setResizable(false);
            stage.sizeToScene();
            /*
            NewCustomerController newCustomerController = LOADER.getController();
            newCustomerController.setFatherGUI(FATHER);
            */
        } catch (IOException e) {
            e.getMessage();
        }
        return stage;
    }
    public static Stage loadEditCustomer(final IndexController FATHER, final Modality MODALITY) {
        Stage stage = new Stage();
        try {
            final FXMLLoader LOADER = new FXMLLoader();
            LOADER.setLocation(Company.class.getResource("EditCustomer.fxml"));
            AnchorPane root = LOADER.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("Editar cliente");
            stage.getIcons().add(new Image(Company.class.getResourceAsStream("asset/img/ico.png")));
            stage.initModality(MODALITY);
            stage.setResizable(false);
            stage.sizeToScene();
            /*
            EditCustomerController editCustomerController = LOADER.getController();
            editCustomerController.setFatherGUI(FATHER);
            */
        } catch (IOException e) {
            e.getMessage();
        }
        return stage;
    }
    public static Stage loadCustomerPhones(final IndexController FATHER, final Modality MODALITY) {
        Stage stage = new Stage();
        try {
            final FXMLLoader LOADER = new FXMLLoader();
            LOADER.setLocation(Company.class.getResource("CustomerPhones.fxml"));
            AnchorPane root = LOADER.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("Teléfonos del cliente");
            stage.getIcons().add(new Image(Company.class.getResourceAsStream("asset/img/ico.png")));
            stage.initModality(MODALITY);
            stage.setResizable(false);
            stage.sizeToScene();
            /*
            CustomerPhonesController customerPhonesController = LOADER.getController();
            customerPhonesController.setFatherGUI(FATHER);
            customerPhonesController.retrievePhones();
            */
        } catch(IOException e) {
            e.getMessage();
        }
        return stage;
    }

}
